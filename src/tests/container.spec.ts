import {ok} from "assert";
import {isFactory, isService} from "../container";

describe('Container', () => {

    describe('isService()', () => {
        it('should works', () => {
            ok(isService(function () {}));
            ok(isService(() => null));
            ok(isService(class {}));
        });

        it('should false for primitives and factories', () => {
            ok(!isService(function cacheFactory() {}));
            ok(!isService(12));
            ok(!isService({}));
            ok(!isService(class logFactory {}));
        });
    });

    describe('isFactory()', () => {
        it('should works', () => {
            ok(isFactory(function cacheFactory() {}));
        });
    });

});