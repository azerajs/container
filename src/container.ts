import {ContainerValue, Factory, IContainer, IDefinition, Service} from "./types";
import {ok} from "assert";
import {isObject} from "util";
import {forEach} from "@azera/util";

const FACTORY_REGEX = /.*Factory$/;

export class Container implements IContainer {

    // Cache for invoked services
    private instances = {};

    /**
     * Get a service or parameter
     * @param {string} name
     * @returns {T}
     */
    get <T>(name: string): T {
        return 2 as any;
    }

    /**
     * Invoke a function and resolve its dependencies
     * @param {T} value
     * @returns {T}
     */
    invoke <T>(value: T): T {
        return 2 as any;
    }

    /**
     * Set services or parameters
     */
    set(values: { [name: string]: ContainerValue } ): this;
    set(name: string, value: ContainerValue): this;
    set(name, value?)
    {
        // Collection of values
        if ( isObject(name) && !value ) {
            return forEach(name, (value, name) => this.set(name, value));
        }

        if ( isFactory(value) ) {

        }
        else if ( isService(value) ) {

        }

        return this;
    }

    addDefinition(definition: IDefinition) {
        ok( isDefinition(definition) , `definition must be an IDefinition`);
        // Defaults
        definition = Object.assign(newDefinition(), definition);
    }

}

/**
 * Create new container definition
 * @returns {IDefinition}
 */
export function newDefinition(): IDefinition {
    return {
        name: null
    };
}

export function isFactory(value): value is Factory { return value instanceof Function && FACTORY_REGEX.test(value.name); }
export function isService(value): value is Service { return value instanceof Function && !FACTORY_REGEX.test(value.name); }
export function isDefinition(value): value is IDefinition {
    return typeof value === 'object' && 'name' in value && 'value' in value;
}