// export type 
export type Factory = Function;
export type Service = Function;
export type ContainerValue = any;

export interface IContainer {

    set(values: { [name: string]: ContainerValue }[] ): this;
    set(name: string, value: ContainerValue): this;

    get <T>(name: string): T;

    invoke <T>(value: T): T;

}

export interface IDefinition {
    name: string;
}